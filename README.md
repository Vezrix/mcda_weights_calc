# Multi-criteria decision-making objective weights calculator 

The application was created in the course of the thesis and then further extended in the [article](https://www.mdpi.com/2073-8994/13/10/1874)

The app is available [here](https://vezrix.pythonanywhere.com/)

## License

Code is provided under [MIT](https://choosealicense.com/licenses/mit/) license