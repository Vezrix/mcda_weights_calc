from flask import Flask, redirect, url_for, render_template, request
from matrix import Matrix
import pandas as pd
import numpy as np
from msg import Msg
from calc_weights import *
from calc_correlation import *

app = Flask(__name__)
data = {}
choosen_weights = []
msg = Msg('', False)
after_upload = False


def format_weights(w):
    idx = 1
    final_str = ""
    for i in w:
        final_str += "<b>C" + str(idx) + "</b>=" + str(round(i, 3)) + "; "
        idx += 1
    return final_str


def calc_weigts():
    global data
    data['matrix'].weights['Mean'] = mean(copy.copy(data['matrix'].values))
    data['matrix'].weights['Standard deviation'] = std_dev(copy.copy(data['matrix'].values))
    data['matrix'].weights['Statistical variance'] = stat_variance(copy.copy(data['matrix'].values))
    data['matrix'].weights['Entropy'] = entropy(copy.copy(data['matrix'].values))
    cilos_w = cilos(copy.copy(data['matrix'].values), data['matrix'].criteria_type)
    critic_w = critic(copy.copy(data['matrix'].values), data['matrix'].criteria_type)
    data['matrix'].weights['CILOS'] = cilos_w
    data['matrix'].weights['CRITIC'] = critic_w
    data['matrix'].weights['IDOCRIW'] = idocriw(cilos_w, critic_w)


@app.route('/', methods=["GET", "POST"])
def home():
    global data
    if request.method == "POST":
        no_a = int(request.form['no_a'])
        no_c = int(request.form['no_c'])
        data['matrix'] = Matrix(no_a, no_c, np.zeros((no_a, no_c), dtype=float), {}, [], {}, {})
        data['matrix'].criteria_type = np.chararray(no_c)
        return redirect(url_for('matrix'))
    return render_template("matrix_dim.html")


@app.route('/matrix', methods=["GET", "POST"])
def matrix():
    global msg
    global after_upload
    global data
    if after_upload:
        after_upload = False
    else:
        msg.text = ''
    if request.method == "POST":
        if request.files['file']:
            file = request.files['file']
            msg.failure = False
            if file.filename.endswith('.csv'):
                try:
                    temp_matrix = pd.read_csv(file, skip_blank_lines=True, skipinitialspace=True, header=None).to_numpy(
                        dtype=float)
                except:
                    msg.text = 'File cannot be read'
                    msg.failure = True
                    raise
            else:
                try:
                    temp_matrix = pd.read_excel(file, header=None).to_numpy(dtype=float)
                except:
                    msg.text = 'File cannot be read'
                    msg.failure = True
                    raise

            after_upload = True

            if temp_matrix.shape[0] != data['matrix'].no_alternatives:
                msg.text = 'Number of alternatives in file is different from chosen'
                msg.failure = True
            elif temp_matrix.shape[1] != data['matrix'].no_criteria:
                msg.text = 'Number of criteria in file is different from chosen'
                msg.failure = True
            else:
                data['matrix'].values = temp_matrix
                msg.text = 'Matrix successfully uploaded, choose criteria type and proceed with next'
                msg.failure = False
            return redirect(request.url)
        else:
            data['matrix'].criteria_type = []
            for i in range(data['matrix'].no_alternatives):
                for j in range(data['matrix'].no_criteria):
                    data['matrix'].values[i][j] = request.form['matrix[' + str(i) + '][' + str(j) + ']']
            for i in range(data['matrix'].no_criteria):
                data['matrix'].criteria_type.append(request.form['type' + str(i)])
            print(data['matrix'].criteria_type)
            calc_weigts()
            return redirect(url_for('weights'))
    return render_template("matrix.html", X=data['matrix'], msg=msg)


@app.route('/weights', methods=["GET", "POST"])
def weights():
    global data
    if request.method == "POST":
        temp_df = pd.DataFrame()
        choosen_weights.clear()
        for i in range(len(request.form)):
            idx = str(request.form['methodw' + str(i)])
            if idx != '-':
                if not np.all(np.isnan(data['matrix'].weights[idx])):
                    choosen_weights.append(idx)
                    temp_df[idx] = data['matrix'].weights[idx]
        if 'Mean' in temp_df.columns:
            data['matrix'].correlations['Pearson'] = temp_df.drop(columns=['Mean']).corr(method='pearson')
        else:
            data['matrix'].correlations['Pearson'] = temp_df.corr(method='pearson')
        data['matrix'].correlations['Weighted Spearman'] = temp_df.rank(ascending=False).corr(method=rw_f)
        data['matrix'].correlations['WS'] = temp_df.rank(ascending=False).corr(method=ws)
        data['matrix'].correlations['Euclidean distance'] = temp_df.rank(ascending=False).corr(
            method=euclidian_distance)
        return redirect(url_for('comparison'))
    return render_template("weights.html", W=data['matrix'].weights, format_weights=format_weights)


@app.route('/comparison', methods=["GET", "POST"])
def comparison():
    global data
    if request.method == "POST":
        temp_corr = {}
        if choosen_weights:
            for i in range(len(request.form)):
                idx = str(request.form['corr_result' + str(i)])
                if idx != '-':
                    if not (len(choosen_weights) == 1 and choosen_weights[0] == 'Mean' and idx == 'Pearson'):
                        temp_corr[idx] = data['matrix'].correlations[idx]
        data['matrix'].final_correlations = temp_corr
        return redirect(url_for('results'))
    return render_template("comparison.html", corrs=data['matrix'].correlations)


@app.route('/results', methods=["GET"])
def results():
    global data
    decision_matrix_df = pd.DataFrame(index=['A' + str(i + 1) for i in range(data['matrix'].values.shape[0])])
    weights_df = pd.DataFrame()

    for i in range(data['matrix'].values.shape[1]):
        decision_matrix_df['C' + str(i + 1)] = data['matrix'].values[:, i]

    if choosen_weights:
        weights_df = pd.DataFrame(index=['C' + str(i + 1) for i in range(data['matrix'].values.shape[1])])
        filtered_weights = {idx: data['matrix'].weights[idx] for idx in choosen_weights}
        for i in filtered_weights:
            weights_df[i] = filtered_weights[i]
    return render_template("results.html", decision_matrix=decision_matrix_df, corrs=data['matrix'].final_correlations,
                           weights=weights_df)


if __name__ == '__main__':
    app.run()
