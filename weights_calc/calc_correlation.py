import numpy as np


def rw_f(rx, ry):
    N = len(rx)
    a = (rx - ry) ** 2
    b = np.array([N - i + 1 for i in rx])
    c = np.array([N - i + 1 for i in ry])
    d = N ** 4 + N ** 3 - N ** 2 - N
    rw = 1 - (6 * np.sum(a * (b + c))) / d
    return rw


def spearman(rx, ry):
    sum = 0.0
    for i in range(0, len(rx)):
        d = rx[i] - ry[i]
        d = d ** 2
        sum += d
    rs = (1 - ((6 * sum) / (len(rx) * (len(rx) ** 2 - 1)))) * 1.0
    return rs


def ws(rx, ry):
    sum = 0
    for i in range(0, len(rx)):
        sum += 2 ** (-rx[i]) * (abs(rx[i] - ry[i]) / max(abs(1 - rx[i]), abs(len(rx) - rx[i])))
    WS = 1 - sum
    return WS


def euclidian_distance(a, b):
    return np.sqrt(np.sum(np.square(a - b)))


def pearson(x, y):
    n = len(x)
    cov_sum = 0
    sd_x_sum = 0
    sd_y_sum = 0
    for i in range(n):
        cov_sum += (x[i] - np.mean(x)) * (y[i] - np.mean(y))
        sd_x_sum += np.square((x[i] - np.mean(x)))
        sd_y_sum += np.square((y[i] - np.mean(y)))
    cov = cov_sum / (n - 1)
    sd_x = np.sqrt(sd_x_sum / n)
    sd_y = np.sqrt(sd_y_sum / n)
    result = cov / (sd_x * sd_y)
    return result
