import numpy as np
import copy
import pandas as pd
from scipy.linalg import null_space
from calc_correlation import *


def cilos(X, criteria_type):
    # Change cost criteria to profit criteria
    x_copy = copy.copy(X)
    for i in range(len(criteria_type)):
        if criteria_type[i] == 'cost':
            for j in range(len(X[:, i])):
                x_copy[j, i] = min(X[:, i]) / X[j, i]
    # Sum normalization
    # for i in range(X.shape[1]):
    #     for j in range(X.shape[0]):
    #         X[j, i] = x_copy[j, i] / sum(x_copy[:, i])
    X = copy.copy(x_copy)
    A = np.zeros([X.shape[1], X.shape[1]])
    # Get max value for each criteria
    for i in range(X.shape[1]):
        A[i, :] = X[np.argmax(X[:, i]), :]
    P = np.zeros([X.shape[1], X.shape[1]])
    for i in range(X.shape[1]):
        for j in range(X.shape[1]):
            if i == j:
                continue
            else:
                P[j, i] = (A[i, i] - A[j, i]) / A[i, i]
    if np.all(P == 0):
        return np.asarray([np.nan] * X.shape[1])
    F = copy.copy(P)
    for i in range(X.shape[1]):
        F[i, i] = -sum(P[:, i])
    # q = Matrix(np.round(F, 4)).nullspace()[0]
    print(F)
    if np.all(F == 0) or np.all(np.isnan(F)):
        return np.asarray([np.nan] * X.shape[1])
    q = null_space(F)
    q = q * np.sign(q[0, 0])
    w = np.zeros(X.shape[1])
    for i in range(X.shape[1]):
        w[i] = q[i] / sum(q)
    return w


def idocriw(entropy_w, cilos_w):
    w = np.zeros(len(entropy_w))
    for i in range(len(entropy_w)):
        w[i] = (entropy_w[i] * cilos_w[i]) / sum(entropy_w * cilos_w)
    return w


def entropy(X):
    # validate_matrix(X, no_alternatives, no_criteria)
    p = np.zeros((X.shape[0], X.shape[1]))
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            p[i, j] = X[i, j] / np.sum(X[:, j])
    print(p)
    E = np.zeros(X.shape[1])
    w = np.zeros(X.shape[1])
    for j in range(X.shape[1]):
        E[j] = -(np.sum(p[:, j] * np.log(p[:, j]))) / np.log(X.shape[0])
    print(E)
    E = 1 - E
    print(E)
    for j in range(X.shape[1]):
        w[j] = (E[j]) / np.sum(E)
    print(w)
    return w


def critic(X, criteria_type):
    x_copy = copy.copy(X)
    print(criteria_type)
    for i in range(len(criteria_type)):
        if criteria_type[i] == 'profit':
            for j in range(len(X[:, i])):
                x_copy[j, i] = (X[j, i] - min(X[:, i])) / (max(X[:, i]) - min(X[:, i]))
        elif criteria_type[i] == 'cost':
            for j in range(len(X[:, i])):
                x_copy[j, i] = (X[j, i] - max(X[:, i])) / (min(X[:, i]) - max(X[:, i]))
    # X = np.around(x_copy, decimals=3)
    # print(X)
    X = copy.copy(x_copy)
    x_df = pd.DataFrame()
    for i in range(len(criteria_type)):
        x_df["C" + str(i + 1) + "_rank"] = X[:, i]
        if criteria_type[i] == 'profit':
            x_df["C" + str(i + 1) + "_rank"] = x_df["C" + str(i + 1) + "_rank"].rank(ascending=False)
        elif criteria_type[i] == 'cost':
            x_df["C" + str(i + 1) + "_rank"] = x_df["C" + str(i + 1) + "_rank"].rank(ascending=True)
    r = np.zeros([X.shape[1], X.shape[1]])
    for i in range(len(criteria_type)):
        for j in range(len(criteria_type)):
            r[i, j] = spearman(np.array(x_df["C" + str(i + 1) + "_rank"].tolist()),
                               np.array(x_df["C" + str(j + 1) + "_rank"].tolist()))
    sd = np.zeros(X.shape[1])
    C = np.zeros(X.shape[1])
    w = np.zeros(X.shape[1])
    for j in range(X.shape[1]):
        x_j = np.sum(X[:, j] / X.shape[0])
        sum_temp = 0
        for i in range(X.shape[0]):
            sum_temp += np.square(X[i, j] - np.abs(x_j))
        sd[j] = np.sqrt(sum_temp / X.shape[0])
        C[j] = sd[j] * sum(1 - r[j])
    for j in range(X.shape[1]):
        w[j] = C[j] / sum(C)
    return w


def mean(X):
    w = np.zeros(X.shape[1])
    for j in range(X.shape[1]):
        w[j] = 1 / X.shape[1]
    return w


def std_dev(X):
    sd = np.zeros(X.shape[1])
    w = np.zeros(X.shape[1])
    for j in range(X.shape[1]):
        sum_temp = 0
        for i in range(X.shape[0]):
            sum_temp += np.square(X[i, j] - np.mean(X[:, j]))
        sd[j] = np.sqrt(sum_temp / X.shape[0])
    for j in range(X.shape[1]):
        w[j] = sd[j] / np.sum(sd)
    return w


def stat_variance(X):
    X_norm = copy.copy(X)

    for i in range(X.shape[1]):
        for j in range(X.shape[0]):
            X_norm[j, i] = X[j, i] / sum(X[:, i])
    X = copy.copy(X_norm)
    V = np.zeros(X.shape[1])
    w = np.zeros(X.shape[1])
    for j in range(X.shape[1]):
        sum_temp = 0
        for i in range(X.shape[0]):
            sum_temp += np.square(X_norm[i, j] - np.mean(X_norm[:, j]))
        V[j] = (1 / X.shape[0]) * sum_temp
    for j in range(X.shape[1]):
        w[j] = V[j] / np.sum(V)
    return w
