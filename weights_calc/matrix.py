from dataclasses import dataclass
import numpy as np


@dataclass
class Matrix:
    no_alternatives: int
    no_criteria: int
    values: np.ndarray
    weights: dict
    criteria_type: []
    correlations: dict
    final_correlations: dict
