from dataclasses import dataclass


@dataclass
class Msg:
    text: str
    failure: bool
